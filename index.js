// console.log("Hello World");

let x = 3;
const result = Math.pow(x, 3)

console.log(`The cube of ${x} is ${result}`);

const fullAddress = {
    brgyName :'Palaslan', 
    provinceName : 'Negros Oriental', 
    countryName : 'Philippines'
};

let {brgyName, provinceName, countryName} = fullAddress
console.log(`I live at ${brgyName} ${provinceName} ${countryName}.`)

const animal = {
    name: 'Uryuu',
    species: 'Quincy',
    weight: '50kg',
    measurement: '6ft',
}

let {name, species, weight, measurement} = animal
console.log(`${name} was a ${species}. He weighed at ${weight} measurement of ${measurement}.`)

const reduceNumber = [1, 2, 3, 4, 5];
reduceNumber.forEach(iterate);
const initialValue = 0;
let sumWithInitial = reduceNumber.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  initialValue
);
console.log(sumWithInitial)
function iterate(reduceNumber){
    console.log(reduceNumber)
}
class Dog{
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog = new Dog('Xander', '32', 'pug');
console.log(dog);